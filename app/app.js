import React from "react";
import {
  TouchableOpacity,
  Image,
  NetInfo,
  StyleSheet,
  View
} from "react-native";
import { RkText, RkStyleSheet } from "react-native-ui-kitten";
import { FontAwesome } from "./../assets/icons";
import { scale } from "./utils/scale";

//import { DrawerNavigator, createStackNavigator } from "react-navigation";
import {
  createAppContainer,
  createStackNavigator,
  createDrawerNavigator
} from "react-navigation";
import { withRkTheme } from "react-native-ui-kitten";
import { AppLoading, Font } from "expo";
//import * as Screens from "./screens";
import splashScreen from "./screens/splashscreen";
import loginScreen from "./screens/loginscreen";
import aboutScreen from "./screens/aboutscreen";
import mainScreen from "./screens/mainscreen";
import detailScreen from "./screens/detailscreen";
import welcomeScreen from "./screens/welcomescreen";
import productLocatorScreen from "./screens/productlocatorscreen";
import pharmacyScreen from "./screens/pharmacyscreen";

import SideMenu from "./navigation/sideMenu";
import { bootstrap } from "./config/bootstrap";
import blogScreen from "./screens/blogscreen";
import UtilColors from './utils/colors';

const thriveNavigation = createStackNavigator(
  {
    Splash: splashScreen,
    Login: loginScreen,
    About: aboutScreen,
    Blog: blogScreen,
    Detail: detailScreen,
    Welcome: welcomeScreen,
    ProductSearch: mainScreen,
    ProductLocatorScreen: productLocatorScreen,
    PharmacyScreen: pharmacyScreen,
    Main: {
      screen: createDrawerNavigator(
        {
          Home: welcomeScreen
        },
        {
          navigationOptions: props => {
            return {
              headerStyle: {
                backgroundColor: UtilColors.titleColor,
              },
              headerTitle: (
                <Image
                  style={{ width: 80, height: 20, flex: 1 }}
                  resizeMode="contain"
                  source={require("./../assets/header_logo.png")}
                />
              ),
              headerLeft: (
                <TouchableOpacity
                  style={{
                    height: "100%",
                    padding: 12,
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "center"
                  }}
                  onPress={() => props.navigation.toggleDrawer()}
                >
                  <RkText rkType="headerButton awesome">
                    {FontAwesome.bars}
                  </RkText>
                </TouchableOpacity>
              ),
              headerRight: (
                <TouchableOpacity
                  style={{
                    height: "100%",
                    padding: 12,
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "center"
                  }}
                  onPress={() => props.navigation.navigate("ProductSearch")}
                >
                  <RkText rkType="headerButton awesome">
                    {FontAwesome.search}
                  </RkText>
                </TouchableOpacity>
              )
            };
          },
          initialRouteName: "Home",
          drawerPosition: "left",
          contentComponent: props => {
            const SideMenuUpdated = withRkTheme(SideMenu);
            return <SideMenuUpdated {...props} />;
          }
        }
      )
    }
  },
  {
    initialRouteName: "Splash"
    // defaultNavigationOptions: {
    //   headerStyle: {
    //     backgroundColor: "#ebcc89"
    //   },
    //   headerTintColor: "#0c0c0c",
    //   headerTitleStyle: {
    //     fontWeight: "bold"
    //   }
    // }
  }
);

ThriveApp = createAppContainer(thriveNavigation);
bootstrap();

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isReady: false,
      block: 0
    };
  }

  componentDidMount() {
    // NetInfo.isConnected.fetch().then(isConnected => {
    //   console.log("First, is " + (isConnected ? "online" : "offline"));
    // });
    console.ignoredYellowBox = ["Setting a timer", "Remote debugger"];
    NetInfo.addEventListener("connectionChange", this.handleConnectivityChange);
  }

  componentWillUnmount() {
    NetInfo.removeEventListener(
      "connectionChange",
      this.handleConnectivityChange
    );
  }

  handleConnectivityChange = isConnected => {
    console.log("------------------------------------------------");
    console.log(isConnected.type);
    //Alert.alert("Development Mode", isConnected.type);
    if (isConnected.type === "none")
      this.setState({
        ...this.state,
        block: 1,
        net: "none",
        status: "No Internet Connection"
      });
    else this.connected(isConnected.type);
  };

  connected = type => {
    if (!!!this.state.status) return;

    this.setState({
      ...this.state,
      block: 2,
      net: type,
      status: "Connecting.."
    });

    this.setState({
      ...this.state,
      block: 3,
      net: type,
      status: "Connected"
    });
    this.timer = setInterval(() => {
      //Alert.alert("Development Mode", "Active Again");
      this.setState({ ...this.state, block: 0 });
      clearInterval(this.timer);
    }, 1500);
  };

  componentWillMount() {
    this.loadFonts();
  }
  async loadFonts() {
    await Font.loadAsync({
      fontawesome: require("./../assets/fonts/fontawesome.ttf"),
      icomoon: require("./../assets/fonts/icomoon.ttf"),
      "Righteous-Regular": require("./../assets/fonts/Righteous-Regular.ttf"),
      "Roboto-Bold": require("./../assets/fonts/Roboto-Bold.ttf"),
      "Roboto-Medium": require("./../assets/fonts/Roboto-Medium.ttf"),
      "Roboto-Regular": require("./../assets/fonts/Roboto-Regular.ttf"),
      "Roboto-Light": require("./../assets/fonts/Roboto-Light.ttf")
    });
    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }
    return (
      <View style={{ flex: 1 }}>
        <ThriveApp />
        {this.state.block > 0 && (
          <View style={styles.offlineContainer}>
            <View
              style={
                this.state.block > 1
                  ? this.state.block > 2
                    ? [styles.content, styles.online]
                    : [styles.content, styles.connecting]
                  : [styles.content]
              }
            >
              <RkText rkType="s6" style={styles.offlineText}>
                {this.state.status}
              </RkText>
            </View>
            <View style={styles.containerBG} />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  toolbarBtn: {
    height: "100%",
    paddingVertical: 12,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  offlineContainer: {
    position: "absolute",
    bottom: 0,
    height: scale(640),
    width: scale(360),
    zIndex: 999
  },
  containerBG: {
    backgroundColor: "#000",
    opacity: 0.3,
    position: "absolute",
    bottom: 0,
    height: scale(640),
    width: scale(360),
    zIndex: 1
  },
  content: {
    position: "absolute",
    backgroundColor: "red",
    width: scale(360),
    height: 25,
    bottom: 0,
    zIndex: 998,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  connecting: {
    backgroundColor: "orange"
  },
  online: {
    backgroundColor: "green"
  },
  icon: {
    fontSize: 35,
    color: "#000",
    marginBottom: 25
  },
  offlineText: { color: "#fff" }
});
