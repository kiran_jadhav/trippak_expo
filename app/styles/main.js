import { StyleSheet } from "react-native";
import { scale } from "./../utils/scale";
import { RkStyleSheet } from "react-native-ui-kitten";

const FontBaseValue = scale(18);
let sizes = {
  h0: scale(32),
  h1: scale(26),
  h2: scale(24),
  h3: scale(20),
  h4: scale(18),
  h5: scale(16),
  h6: scale(15),
  p1: scale(16),
  p2: scale(15),
  p3: scale(15),
  p4: scale(13),
  s1: scale(15),
  s2: scale(13),
  s3: scale(13),
  s4: scale(12),
  s5: scale(12),
  s6: scale(13),
  s7: scale(10),
  base: FontBaseValue,
  small: FontBaseValue * 0.8,
  medium: FontBaseValue,
  large: FontBaseValue * 1.2,
  xlarge: FontBaseValue / 0.75,
  xxlarge: FontBaseValue * 1.6
};

export let customStyles = RkStyleSheet.create(theme => ({
  containerSuper: {
    flex: 1,
    alignItems:"center"
  },
  container: {
    flex: 1,
    margin: 8,
    alignItems: "center",
    justifyContent: "center"
  },
  containerContent: {
    flex: 1,
    margin: 20,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  searchContainer: {
    backgroundColor: theme.colors.screen.bold,
    paddingHorizontal: 16,
    paddingVertical: 10,
    height: 60,
    alignItems: "center"
  },
  imageFull: {
    width: scale(200),
    height: scale(200),
    resizeMode:"contain"
  },
  image: {
    width: 140,
    height: 105
  },
  imageAbtSquare:{
    width: scale(300),
    height: scale(180),
    marginTop:20
  },
  imageAbt: {
    width: scale(240),
    height: scale(240),
    borderRadius: scale(120), 
    marginTop:20
  },
  imagecircle:{
    marginTop:20,
    width: scale(240),
    height: scale(240),
    borderRadius: scale(240/2),
    borderColor: "#cccccc", 
    borderWidth: 2
  },
  heading: {
    fontSize: sizes.h1,
    marginTop: 12,
    textAlign:"center"
  },
  subheading: {
    fontSize: sizes.h2,
    marginTop: 10,
    textTransform:"uppercase",
    textAlign:"center"
  },
  field:{
    borderBottomWidth: 0.5,
    borderWidth: 0.5,
    borderBottomColor: "#e5e5e5",
    borderColor: "white",
  },
  error:{
    borderBottomWidth: 0.5,
    borderWidth: 0.5,
    borderBottomColor: "red",
    borderColor: "red",
  },
  text: {
    fontSize: sizes.small,
    marginTop: 8,
    textAlign:'center',
    lineHeight: scale(24),
    marginBottom: scale(24)
  },
  textError: {
    fontSize: sizes.small,
    color: "#FF0000",
    marginTop: 8
  },
  inputField: {
    fontSize: sizes.small,
    borderWidth: 2, // size/width of the border
    borderColor: "lightgrey", // color of the border
    margin: scale(5),
    paddingLeft: 10,
    width: scale(250)
  },
  inputButton: {
    fontSize: sizes.small,
    margin: scale(5),
    width: scale(250)
  },
  bottomBar: {
    backgroundColor: "#333",
    position: "absolute",
    left: 0,
    bottom: 0,
    width: scale(350),
    height: 40,
    zIndex: 999
  },
  bottomBarText: {
    flex:1,
    width: scale(350),
    paddingVertical:10,
    textAlign: "center",
    color: "#fff"
  },
  locator:{
    marginTop: 12,
    backgroundColor: "#008390",
    borderWidth: 0.5, // size/width of the border
    borderColor: "#008390", // color of the border
    textAlign: "center",
    padding:8,
    width: scale(320),
    borderRadius:0,
    marginBottom:20
  },
  productImages:{
    paddingHorizontal:8,
    flex: 1, 
    flexDirection: 'row',
  },
  productImage:{
    paddingVertical:4,
    width: scale(100),
    height: scale(100),
    resizeMode:"contain"
  },
  shopBtn:{
    // backgroundColor:"#EBCC89",
        backgroundColor:"#008390",
    marginTop: 20,
    width: scale(230)
  },
  imagePopup:{
    backgroundColor: "#333",
    position: "absolute",
    left: 0,
    top: 0,
    width: scale(350),
    height: scale(240),
    zIndex: 999
  }
}));
