import React from "react";
import {
  KeyboardAvoidingView,
  Keyboard,
  Text,
  View,
  TextInput,
  Button,
  StatusBar,
  Image,
  Dimensions ,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
  Modal 
} from "react-native";
import { NavigationActions ,HeaderBackButton} from "react-navigation";
import { customStyles } from "./../styles/main";
import { RkButton, RkTextInput, RkText } from "react-native-ui-kitten";
import { FontAwesome } from "./../../assets/icons";
import _ from "lodash";
import { data } from "./../data";
import firebase from "./../FirebaseConnect";
import ImageZoom from 'react-native-image-pan-zoom';
import { scale } from "../utils/scale";
//import { Icon } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImageViewer from 'react-native-image-zoom-viewer';

var database = firebase.database();

export default class productLocatorScreen extends React.Component {
  // static navigationOptions = {
  //   title: "Login"
  // };
  static navigationOptions = props => ({

    // headerTitle: "PRODUCT LOCATOR",
    // headerTitleStyle: { color: "black" },
    // headerTintColor: "black",
    // headerStyle: {
    //   backgroundColor: "white"
    // }
    header: null,
    headerMode: "none"
    
  });

  constructor(props) {
    super(props);
    this.state = {
      fullImage: false,
      image1show: true,
      image:""
    };

  }



  handleZoom = (imageType) => {

    this.setState({fullImage:true});
  }

  closeZoomScreen=()=>{
    console.log("ss");
    this.setState({fullImage:false});
  }

  componentDidMount = () => {
  };

  render() {
    var images = [{
    // Simplest usage.
    url: this.props.navigation.getParam("productLocator", require("./../../assets/noimagefound.png")),

    // width: number
    // height: number
    // Optional, if you know the image size, you can set the optimization performance

    // You can pass props to <Image />.
    props: {
        // headers: ...
        
      }
    }]
    return (

      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
      }}>
      <View style={{marginTop:35}}>
      <RkText style={{fontSize: 18}}>{this.props.navigation.getParam("name")}</RkText>
      </View>
      <View >
      {!this.state.fullImage && 
        <TouchableOpacity onPress={() => this.handleZoom(1)}>
        <Image style={{resizeMode: "contain",
        width:scale(350), height:scale(450),}} source={{uri: this.props.navigation.getParam("productLocator")}}/>
        </TouchableOpacity>}
        </View>

        <View style={{marginBottom:10}}>
        <TouchableWithoutFeedback
        style={{ marginBottom: 0 }}
        onPress={() => Linking.openURL(config.virtualPhysician)}
        >
        <Image
        style={{ width: 130, height: 50 }}
        resizeMode="contain"
        source={require("./../../assets/header_logo.png")}
        />
        </TouchableWithoutFeedback>
        <RkButton
        onPress={() => this.props.navigation.dispatch({ type: "Navigation/BACK" })}
        style={{
          backgroundColor: "#000",
          borderRadius: 0,
          marginBottom:5
        }}
        >
        CLOSE
        </RkButton>
        </View>
        {this.state.fullImage && 
         <Modal  visible={true} transparent={true} onRequestClose={() => { this.visibleModal(false); } }>
         <ImageViewer 

         maxOverflow={0}
         renderIndicator = {(currentIndex, allSize) => <Text></Text>}
         renderFooter = {() => (

          <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={{width: "35%"}} />
          <RkButton
          onPress={() => this.closeZoomScreen()}
          style={{
            marginTop: 12,
            backgroundColor: "#ebcc89",
            borderWidth: 0.5, // size/width of the border
            borderColor: "#ebcc89", // color of the border
            textAlign: "center",
            padding:8,
            width: "30%",
            borderRadius:0,
            marginBottom:20
          }}
          >
          CLOSE
          </RkButton>
          <View style={{width: "35%"}} />

          </View>

          )}
         imageUrls={images}/>
         </Modal>}
         </View>


         );
  }
}
