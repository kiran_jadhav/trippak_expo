import React from "react";
import { AppLoading, Font } from "expo";

import { KittenTheme } from "./../config/theme";
import { scale, scaleVertical } from "./../utils/scale";
import { StackActions, NavigationActions } from "react-navigation";
import { StyleSheet, Image, View, Dimensions, StatusBar } from "react-native";
import { RkText, RkTheme } from "react-native-ui-kitten";

const delay = 0;

export default class splashScreen extends React.Component {
  static navigationOptions = {
    header: null,
    headerMode: "none"
  };

  state = {
    progress: 0
  };

  componentDidMount() {
    StatusBar.setHidden(true, "none");
    RkTheme.setTheme(KittenTheme);
    this.timer = setInterval(this.updateProgress, delay);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  updateProgress = () => {
    if (this.state.progress === 1) {
      clearInterval(this.timer);
      setTimeout(this.onLoaded, delay);
    } else {
      const randProgress = this.state.progress + Math.random() * 0.5;
      this.setState({ progress: randProgress > 1 ? 1 : randProgress });
    }
  };

  onLoaded = () => {
    StatusBar.setHidden(false, "slide");
    const toHome = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Login" })]
    });
    this.props.navigation.dispatch(toHome);
  };

  render = () => (
    <View style={styles.container}>
      <View>
        
        <View style={styles.text}>
        <Image
          style={[styles.image, { width: scale(0) }]}
          source={require("./../../assets/logo.png")}
        />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ebcc89",
    flex: 1
  },
  image: {
    resizeMode: "contain",
    marginTop: 140,
    width:scale(250),
    height: scaleVertical(230)
  },
  text: {
    justifyContent:"center",
    alignItems: "center",
  },
  hero: {
    fontSize: 37
  },
  appName: {
    fontSize: 62
  },
  progress: {
    alignSelf: "center",
    marginBottom: 35,
    backgroundColor: "#e5e5e5"
  }
});
