import React from "react";
import {
  FlatList,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  ScrollView,
  Keyboard,
  Platform,
  TouchableHighlight,
  Linking,
  StatusBar,
  Button
} from "react-native";
import _ from "lodash";
import { RkStyleSheet, RkText, RkTextInput,  RkButton } from "react-native-ui-kitten";
import { NavigationActions, HeaderBackButton } from "react-navigation";
import { data } from "./../data";
import { FontAwesome } from "../../assets/icons";
import HTMLView from 'react-native-htmlview';
import { scale } from "./../utils/scale";
import { customStyles } from "../styles/main";
const { height, width } = Dimensions.get("window");
import firebase from "./../FirebaseConnect";
var database = firebase.database();

export default class pharmacyScreen extends React.Component {
  static navigationOptions = props => ({
    headerLeft: (
      <HeaderBackButton
      tintColor="#000"
      onPress={() => props.navigation.dispatch({ type: "Navigation/BACK" })}
      />
      ),
    headerTitle: "PHARMACY",
    headerTitleStyle: { color: "black" },
    headerTintColor: "black",
    headerStyle: {
      backgroundColor: "#ebcc89"
    }
  });

  state = {
    data: {
      categoryOriginal: null,
      categoryFilterd: null,
      original: null,
      filtered: null
    },
    searchResults: false,
    defaultSearchText: ""
  };

  componentDidMount = () => {
    this.getRemoteProduct();
  };
  getRemoteProduct() {
    database.ref("products").on("value", snapshot => {
      let products = [];
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        
        if(typeof childData.uses !== 'undefined'){

          products.unshift(childData);
          if (childData.name == "Advil PM") {
            products.unshift(
            {
              "categories" : [ "1", "5", "7", "26", "41", "80", "81", "82", "83", "84", "118", "119" ],
              "description" : " Uses:\r\nTemporarily relieves theses symptoms due to hay fever or other upper respiratory allergies: <ul><li>Runny Nose</li><li>&nbsp;Sneezing</li><li>Itchy, watery eyes&nbsp;</li><li>Itching of the nose and throat&nbsp;</li><li>Temporarily relieves the symptoms of the common cold</li><li>Runny nose</li><li>Sneezing</li></ul>",
              "embedVideo" : "https://youtu.be/6cbOc_4t1NE",
              "id" : "30",
              "name" : "Benadryl Allergy",
              "order" : 95,
              "photos" : [ "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/BND-LLR-1.png?alt=media&token=43c6cb6b-6f5f-4018-9bde-67ee76656522", "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/BND-LLR-2.jpg?alt=media&token=2170751c-1193-4ce7-a1b0-34aede65d461" ],
              "price" : "6.00",
              "product_locator" : "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/productlocator%2Fleft_new%2FBND-LLR.jpg?alt=media&token=dceb223e-2061-49c7-b836-c625c25f39b1",
              "product_url" : "",
              "sku" : "BND-LLR",
              "uses" : "<h4>Benadryl Allergy</h4><ul><li>Sleep agent when itching at night</li></ul>"
            })
          }


          if (childData.name == "Miconazole") {
            products.unshift(
            {
              "categories" : [ "5", "7", "15", "26", "36", "76", "77", "78", "79", "95", "98", "99", "117" ],
              "description" : "Anti-Itch cream with calming Aloe Vera\r\nUses:\r\n <ul><li>Temporarily relieves itching associated with minor skin irritations, inflammation and rashes due to:</li><li>Eczema</li><li>Detergents</li><li>Cosmetics</li><li>Psoriasis</li><li>Jewelry</li><li>Soaps</li><li>Poison Ivy, Oak, Sumac</li><li>Insect bites</li><li>Seborrheic dermatitis</li><li>Temporarily relieves external anal and genital itching</li></ul>",
              "embedVideo" : "https://youtu.be/dfbgVCPIbcE",
              "id" : "27",
              "name" : "Cortizone 10",
              "order" : 125,
              "photos" : [ "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/CRT-10-1.jpg?alt=media&token=1cb7390e-fa95-481a-963e-909c53e1a5db", "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/CRT-10-2.jpg?alt=media&token=5ffa2e15-1749-4d5e-9691-2cb75bbc8e95", "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/CRT-10-3.jpg?alt=media&token=3d216584-813b-415b-95ee-273f212e84df" ],
              "price" : "6.00",
              "product_locator" : "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/productlocator%2Fright_new%2FCRT-10-PL.jpg?alt=media&token=f8c912ea-597b-4f13-ac6e-e8b774365dd5",
              "product_url" : "",
              "sku" : "CRT-10",
              "uses" : "<h4>Cortizone 10</h4><h5>Anti-Itch cream with calming Aloe vera<br>Uses:</h5><ul><li><i>Temporarily relieves itching associated with minor skin irritations,inflammation and rashes due to:</i></li><li>Eczema</li><li>Detergents</li><li>Cosmetics</li><li>Psoriasis</li><li>Jewelry</li><li>Soaps</li><li>Poison ivy,oak,sumac</li><li>Insect bites</li><li>Seborrheic dermatitis</li><li>Temporarily relieves external anal and genital itching</li></ul>"
            })
          }

          //console.log(childData.categories);
        }
        
      });
      this.setState(
      {
        ...this.state,
        data: {
          ...this.state.data,
          original: products,
          filtered: _.sortBy(products, o => o.order)
        }
      },
      );
    });
}

showProductLocatorScreen(item){
  this.props.navigation.navigate("ProductLocatorScreen",{productLocator:item.product_locator,name:item.name.toUpperCase()});
}



renderItem = ({ item }) => (
  <View>
  <View>
    <HTMLView
    stylesheet={htmlstyles}
    value={item.uses}
    renderNode={this.renderNode}
    />
  </View>
   <View style={{alignItems:"center"}}>
   <Text onPress={()=>this.showProductLocatorScreen(item)} style={[{ color: "white" ,fontSize:16 ,textAlign:"center", marginBottom:20, marginTop:15, borderColor: '#ffffff', borderWidth:0.5, borderRadius: 4, paddingHorizontal:20, 
       paddingVertical:8,
       shadowColor: "#fff",
       shadowOffset: {width: 0, height: -3.5,},
       shadowOpacity: 0.30,
       shadowRadius: 3,
       elevation: 3, 
       backgroundColor:'#000'
       }]}>Product Locator</Text>   
    </View>
  </View>
  );


renderNode(node, index, siblings, parent, defaultRenderer) {
  if (node.name == "li") {
    return (
      <View key={index} style={{ marginTop: index === 0 ? 0 : 0 }}>
      <View style={{ flexDirection: "row" }}>
      <View
      style={{ flexDirection: "column", marginLeft: 12, width: 20 }}
      >
      <Text style={{color: "white",fontSize:18,}}>{"\u2022"}</Text>
      </View>
      <View style={{ flexDirection: "column", width: "90%" ,color: "white"}}>
      <Text style={{color: "white",fontSize:18,}}>{defaultRenderer(node.children, parent)}</Text>
      </View>
      </View>
      </View>
      );
  } else if (node.name == "ul") {
    return <View>{defaultRenderer(node.children, parent)}</View>;
  }
}
render() {
  return (
    <View style={[customStyles.containerSuper, { backgroundColor: "#000" }]}>
    <ScrollView>
    <View
    style={[
      { backgroundColor: "#000", color: "#fff" }
      ]}
      >

      <Text style={[customStyles.subheading, { color: "#0098a7", fontWeight: "bold" ,marginBottom:10}]}>{"\n"}WELCOME TO TRIPPAK PHARMACY</Text>
        <View style={{flex:1, flexDirection:'column', backgroundColor:'#ffffff', marginLeft:2.5, marginRight:2.5}}>
           <Image source={require('../../assets/ver-side.png')} style={{width: '100%', height:scale(250)}} resizeMode="contain" />
        </View>

      {/* <View style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'stretch',
        marginTop:5,
        marginBottom:10,
        marginLeft:2.5, 
        marginRight:2.5
        }}>
          <Image source={require('../../assets/left-side.png')} style={{width: '49%', height:220, backgroundColor:'#ffffff', marginRight:2.5}} resizeMode="contain" />
          <Image source={require('../../assets/right-side.png')} style={{width: '49%', height:220, backgroundColor:'#ffffff', marginLeft:2.5}} resizeMode="contain" />
        </View> */}

        <Text style={[customStyles.subheading, { color: "#0098a7", fontWeight: "bold" ,marginBottom:10}]}>SELECT WHAT YOU NEED</Text>

        <Text style={{ color: "#fff", textAlign:'center', fontSize:18, paddingHorizontal:10, marginBottom:20}}>Browse from sections below and use locator to select product you desire.</Text>

        <FlatList
        style={styles.root}
        data={this.state.data.filtered}
        renderItem={this.renderItem}

        //ListHeaderComponent={this.renderHeader}
        //stickyHeaderIndices={}
        //ItemSeparatorComponent={this.renderSeparator}
        keyExtractor={this.extractItemKey}
        horizontal={false}
        numColumns={1}
        enableEmptySections
        />

        </View>
        </ScrollView>

        </View>
        );
}
}
const styles = RkStyleSheet.create(theme => ({
  root: {
    paddingLeft:15,
    paddingRight:15,
    backgroundColor: 'black',
    marginBottom: 10
  },

  container: {
    flexDirection: "column",
    alignItems: "center"
  },
  
  title: {
    fontSize: 13,
    width: (width - 32) / 2,
    height: 20,
    alignItems: "center",
    textAlign: "center",
    color: "#fff"
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: theme.colors.border.base
  },
  pl_btn:{
    color: "white",
    backgroundColor: "#0098a7" , //Blue
    fontSize:16 ,
    textAlign:"center", 
    marginBottom:20, 
    marginTop:10, 
    borderColor: '#0098a7', 
    borderWidth:0.5, 
    borderRadius: 4, 
    paddingHorizontal:20, 
    paddingVertical:8
  },
  locatorButton:
    {
       borderWidth: 0.5, 
       borderRadius: 4,
       borderColor: '#0098a7',
       color:'#0098a7',
       width: 160,
       padding: 2,
       backgroundColor: '#0098a7',
       marginBottom:20, 
       marginTop:15
    },

    locatorButtonText:
    {
        textAlign: 'center',
        color: '#fff',
        paddingVertical:6,
        fontSize:16
    }
  
}));

var fontSize=18;
var htmlstyles =StyleSheet.create({
  a: {
    fontWeight: '300',
    fontSize:fontSize
  },
  p:{
    color:'white',
    fontSize:fontSize,
    marginTop:0,
    marginBottom:0
  },
  strong:{
    fontWeight:'bold',
    fontSize:fontSize
  },
  li:{
    marginBottom:0,
    fontSize:fontSize,
    color:'white',
  },
  h4:{
    fontSize:20,
    color:'white',
    marginTop:-10,
    marginBottom:Platform.OS === 'ios' ? -20 : 0,
    textDecorationLine:'underline'
  },
  h5:{
    fontSize:18,
    color:'white',
    marginBottom: Platform.OS === 'ios' ? -15 : 0,
  },
  h2:{
    fontSize:22,
    color:'#0098a7',
    lineHeight:22,
    marginTop:10,
    marginBottom: Platform.OS === 'ios' ? 0 : 15,
    fontWeight:'bold'
  },
  ul:{
    fontSize:fontSize,
    color:'white',
    marginBottom:-12,
    paddingVertical:0
  },
  br:{
    height:15
  }
})