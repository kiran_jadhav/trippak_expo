import React from "react";
import {
  KeyboardAvoidingView,
  Keyboard,
  Text,
  View,
  TextInput,
  Button,
  StatusBar,
  Image
} from "react-native";
import { NavigationActions } from "react-navigation";
import { customStyles } from "./../styles/main";
import { RkButton, RkTextInput, RkText } from "react-native-ui-kitten";
import { FontAwesome } from "./../../assets/icons";
import _ from "lodash";
import { data } from "./../data";
import firebase from "./../FirebaseConnect";

var database = firebase.database();

export default class loginScreen extends React.Component {
  // static navigationOptions = {
  //   title: "Login"
  // };
  static navigationOptions = {
    header: null,
    headerMode: "none"
  };

  constructor(props) {
    super(props);
    this.state = {
      data: {
        username: "", // User ID
        password: "" // Password
      },
      users: [
        {
          username: "demo@demo.com", // User ID
          password: "demo" // Password
        },
        {
          username: "fdhmdinc2@gmail.com", // User ID
          password: "Fdhmd1234!!" // Password
        }
      ],
      loading: false,
      errors: {}
    };
  }



  componentDidMount = () => {

    //this.getRemoteCategories();
    //console.log(data.getProducts());
    // console.log("-----");
    // console.log(console.log(this.findAll("43", data.getProducts())));
    // console.log("-----");

  };



  validate = data => {
    const errors = {};
    if (!data.username) errors.username = "Can't be blank";
    if (!data.password) errors.password = "Can't be blank";
    return errors;
  };

  navigateScreen = _screen => {
    this.props.navigation.reset(
      [NavigationActions.navigate({ routeName: _screen })],
      0
    );
  };

  onChange = (field, value) => {
    //console.log(field, value);
    this.setState({
      data: { ...this.state.data, [field]: value }
    });
  };

  onSubmit = () => {
   //this.navigateScreen("PharmacyScreen");
   
   const errors = this.validate(this.state.data);
    this.setState({ errors });

    console.log(this.state);
    if (Object.keys(errors).length === 0) {

      this.state.users.map((user) => {
        if (
          this.state.data.username == user.username &&
          this.state.data.password == user.password
        ) {
          this.navigateScreen("Main");
        }
      })

    //  console.log("Show Error");
      this.setState({
        errors: {
          result: "Invalid Username or Password"
        }
      });

      // if (
      //   this.state.data.username == this.state.data.password &&
      //   this.state.data.username == "demo"
      // ) {
      //   this.navigateScreen("Main");
      // } else {

      // }
    }
  };

  render() {
    return (
      <KeyboardAvoidingView
        behavior={"padding"}
        style={customStyles.container}
        onPress={() => Keyboard.dismiss()}
        enabled={true}
      >
        <View style={customStyles.container}>
          <StatusBar barStyle="light-content" backgroundColor="#6a51ae" />
          <Image
            style={[customStyles.image]}
            onPress={() => Keyboard.dismiss()}
            source={require("./../../assets/logo.png")}
          />
          <Text style={[customStyles.text, { marginBottom: 20 }]}>
            Enter username and password to login
          </Text>
          <RkTextInput
            style={!!this.state.errors.username ? customStyles.error : customStyles.field}
            name="username"
            onChangeText={text => this.onChange("username", text)}
            editable={true}
            keyboardType={"email-address"}
            placeholder={"Username"}
            autoCapitalize="none"
            maxLength={40}
            onSubmitEditing={event => {
              {
                !!this.password && this.password.focusInput();
              }
            }}
            blurOnSubmit={false}
            returnKeyType="next"
          />
          <RkTextInput
            style={!!this.state.errors.password ? customStyles.error : customStyles.field}
            ref={input => {
              this.password = input;
            }}
            name="password"
            onChangeText={text => this.onChange("password", text)}
            placeholder={"Password"}
            secureTextEntry={true}
            maxLength={40}
            onSubmitEditing={event => {
              this.onSubmit();
            }}
          />
          <Text style={[customStyles.textError]}>
            {!!this.state.errors.result && "Invalid Username or password\n"}
          </Text>


          <RkButton onPress={this.onSubmit} style={{ backgroundColor: "#EBCC89" }}> LOGIN </RkButton>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
