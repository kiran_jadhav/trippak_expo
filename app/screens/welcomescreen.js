import React from "react";
import {
  TouchableHighlight,
  TouchableOpacity,
  Text,
  View,
  Image,
  Linking,
  StatusBar,
  ScrollView,
  SafeAreaView,
  ImageBackground
} from "react-native";
import { NavigationActions } from "react-navigation";
import { RkText, RkStyleSheet, RkButton } from "react-native-ui-kitten";
import { customStyles } from "../styles/main";
import { FontAwesome } from "../../assets/icons";
import { bold } from "ansi-colors";
import { scale } from "./../utils/scale";
import { vw, vh, vmin, vmax } from 'react-native-expo-viewport-units';
import UtilColors from '../utils/colors';

export default class welcomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      title: "New Post",
      headerStyle: {
        backgroundColor: UtilColors.titleColor,
      },
      headerTitle: (
        <Image
          style={{ width: 100, height: 100, flex: 1 }}
          resizeMode="contain"
          source={require("./../../assets/header_logo.png")}
        />
      ),
      headerRight: (
        <TouchableOpacity
          style={styles.toolbarBtn}
          onPress={() => params._OnSubmit()}
        >
          <RkText rkType="headerButton awesome">{FontAwesome.bars}</RkText>
        </TouchableOpacity>
      ),
      headerLeft: (
        <TouchableOpacity style={styles.toolbarBtn}>
          <RkText rkType="headerButton awesome" />
        </TouchableOpacity>
      )
    };
    // headerLeft: <View />
  };

  replenishProduct = () => {
    //data.getRemoteCategories();
    Linking.openURL("https://trippak.health");
  };

  componentDidMount = () => {
    StatusBar.setBarStyle("dark-content");
  };

  render() {
    return (
      <View style={{ backgroundColor: "#000" }}>
        <ScrollView>
          <View style={[customStyles.container, { margin: 0 }]}>
            <ImageBackground
              source={require("../../assets/welcome_bg.png")}
              style={{
                flex: 1,
                //width: scale(360),
                // height: scale(440),
                width: vw(100), 
                height: vh(85),
                alignItems: "center",
                justifyContent: "center",
                marginBottom: 20
              }}
            >
              <View
                style={{
                  backgroundColor: "#44352c40",
                  padding: 8,
                  flex: 1,
                  width: scale(360),
                  height: scale(440),
                  alignItems: "center",
                  justifyContent: "center",
                  
                }}
              >
                <Text
                  style={[
                    customStyles.subheading,
                    { fontSize: 30, fontWeight: "900", textTransform:"uppercase",  },
                    { color: "#fff" }
                  ]}
                >
                  WELCOME TO TRIPPAK{"\u2122"}
                </Text>

                <Text
                  style={[
                    customStyles.text,
                    { fontSize: 20, fontWeight: "bold", paddingLeft: 10, paddingRight: 10 },
                    { color: "#fff" }
                  ]}
                >
                  Dedicated to supporting your health in your time of need!
                </Text>
                <RkButton
                  
                  style={[customStyles.shopBtn]}
                  onPress={() =>
                    this.props.navigation.navigate("ProductSearch")
                  }
                >
                  SEARCH BY SYMPTOM
                </RkButton>
              </View>
            </ImageBackground>
            <Text style={[customStyles.subheading,{ fontWeight: "bold", color: "#0098a7" }]}>Welcome</Text>
            <Text style={[customStyles.text, { color: "#fff", paddingHorizontal:10, marginBottom:0 }]}>There's  so much to see here. So, take your time, look around, and learn all there is to know about this product.</Text>
            <Image
              style={[customStyles.imageAbtSquare, { marginBottom: 30 }]}
              source={require("../../assets/welcome_image.png")}
            />
            <Text
              style={[
                customStyles.subheading,
                { fontWeight: "bold" },
                { color: "#0098a7" }
              ]}
            >
              Detail our Product
            </Text>
            <Text style={[customStyles.text, { color: "#fff", paddingHorizontal:10 }]}>
               Trip Pak{"\u2122"} is the first over the counter (OTC) medicine bag containing a virtual portable pharmacy. This patent pending  Medicine Organizer is complete in every aspect and contains 35 unique branded products "for all that ails you" . The medications are housed conveniently for fast and efficient removing and re-inserting product at point of use.
            </Text>
            
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'stretch', backgroundColor:'#ffffff', marginLeft:2.5, marginRight:2.5}}>
               <Image source={require('../../assets/ver-side.png')} style={{width: '100%', height:scale(240)}} resizeMode="contain" />
            </View>
             
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'stretch', marginBottom:10, marginTop:5, marginLeft:2.5, marginRight:2.5}}>
              <Image source={require('../../assets/left-side.png')} style={{width: '49%', height:220, backgroundColor:'#ffffff', marginRight:2.5}} resizeMode="contain" />
              <Image source={require('../../assets/right-side.png')} style={{width: '49%', height:220, backgroundColor:'#ffffff', marginLeft:2.5}} resizeMode="contain" />
            </View>
            
            <Text style={[customStyles.text, { color: "#fff", paddingHorizontal:10 }]}>
               Visit our pharmacy for known products or use this App to help you select the appropriate medication based on your symptom(s).
            </Text>
            
            <Text style={[customStyles.text, { color: "#fff", marginBottom:20, paddingBottom:20, marginTop:0, paddingHorizontal:10 }]}>AS WITH ALL MEDICATIONS, INDICATIONS AND USAGE, DOSAGE AND ADMINISTRATION, WARNINGS AND PRECAUTIONS, DRUG INTERACTIONS, AND ADVERSE REACTIONS FOR SPECIFIC PRODUCTS VARY AND THE FULL PRESCRIBING INFORMATION FOR THE INDIVIDUAL PRODUCT MUST BE CONSULTED. THESE PRODUCTS ARE NOT INTENDED TO DIAGNOSE, TREAT, CURE OR PREVENT ANY DISEASE.</Text>
           


            
          </View>
        </ScrollView>
        <TouchableHighlight
          style={customStyles.bottomBar}
          onPress={this.replenishProduct}
        >
          <Text style={customStyles.bottomBarText}>
            Click here to replenish a product
          </Text>
        </TouchableHighlight>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(theme => ({
  toolbarBtn: {
    height: "100%",
    paddingVertical: 12,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  }
}));
