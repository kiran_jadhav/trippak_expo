import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  Linking,
  ScrollView
} from "react-native";
import { NavigationActions, HeaderBackButton } from "react-navigation";
import { customStyles } from "../styles/main";
import _ from "lodash";
import firebase from "./../FirebaseConnect";
import HTMLView from "react-native-htmlview";
import UtilColors from '../utils/colors';
import { scale } from "../utils/scale";
var database = firebase.database();

export default class blogScreen extends React.Component {
  constructor(props) {
   super(props);
   this.state = {
    data: [],
    image_style:customStyles.imageAbtSquare
  };

  
}

static navigationOptions = props => ({
  headerLeft: (
    <HeaderBackButton
    tintColor="#000"
    onPress={() => props.navigation.dispatch({ type: "Navigation/BACK" })}
    />
    ),
  headerTitle: props.navigation.state.params.title || "",
  headerTitleStyle: { color: "black" },
  headerTintColor: "black",
  headerStyle: {
    backgroundColor: UtilColors.titleColor,
  }
});



replenishProduct = () => {
  Linking.openURL("https://trippak.health");
};

componentDidMount = () => {
  const product = this.props.navigation.getParam("id", 1);
  console.log("-------------------" + product);
  this.getRemoteProduct(product);
  
};

getRemoteProduct(_id) {
  database.ref("blog").on("value", snapshot => {
    let blogs = [];
    snapshot.forEach(function(childSnapshot) {
      var childKey = childSnapshot.key;
      var childData = childSnapshot.val();
      blogs.unshift(childData);
    });
    console.log("Search for blog id" + _id);
    this.setState(
    {
      ...this.state,
      data: _.find(blogs, x => x.id === _id)
    },
    () => {
      console.log("->" + typeof this.state.data.blog);

      this.props.navigation.setParams({ title: this.state.data.title });
      if (this.state.data.title == "FAQs" ||
        this.state.data.title == "Testimonials") {
        this.setState({image_style:styles.imageFaqs});
      }
    }
    );
  });


}

renderNode(node, index, siblings, parent, defaultRenderer) {
  if (node.name == "li") {
    return (
      <View key={index} style={{ marginTop: index === 0 ? 10 : 0 }}>
      <View style={{ flexDirection: "row" }}>
      <View
      style={{ flexDirection: "column", marginLeft: 20, width: 20 }}
      >
      <Text>{"\u2022"}</Text>
      </View>
      <View style={{ flexDirection: "column", width: "90%" }}>
      <Text>{defaultRenderer(node.children, parent)}</Text>
      </View>
      </View>
      </View>
      );
  } else if (node.name == "ul") {
    return <View>{defaultRenderer(node.children, parent)}</View>;
  }
}

render() {
  return (
    <View style={[customStyles.containerSuper, { backgroundColor: "#000" }]}>
    {!!this.state.data.blog && (
      <ScrollView alwaysBounceHorizontal={false}>
      <View
      style={[
        customStyles.containerContent,
        { backgroundColor: "#000", color: "#fff" }
        ]}
        >
        {!!this.state.data.image && (
          <Image
          style={[
            this.state.image_style,
            { borderColor: "#cccccc", borderWidth: 2, marginBottom: 51 }
            ]}
            source={{ uri: this.state.data.image }}
            />
            )}

        <HTMLView
        style={{ width: scale(305) }}
        stylesheet={styles}
        value={"<body>" + this.state.data.blog + "</body>"}
        />
        </View>
        </ScrollView>
        )}
    <TouchableHighlight
    style={customStyles.bottomBar}
    onPress={this.replenishProduct}
    >
    <Text style={customStyles.bottomBarText}>
    Click here to replenish a product
    </Text>
    </TouchableHighlight>
    </View>
    );
}
}
const styles = StyleSheet.create({
  body: {
    color: "#fff",
    textAlign: "center"
  },
  p: {
    paddingBottom: 10
  },
  ul: {
    paddingLeft: 10,
    paddingBottom: 10,
    fontSize: 10
  },
  li:{
    marginTop:20,
    color:'white',fontSize: 15
  },
  sup:{
    fontSize:10,
    flexDirection: 'row', alignItems: 'flex-start'
  },
  imageFaqs:{
    width: scale(300),
    height: scale(200),
    marginTop:20,
    resizeMode: "contain",
    borderWidth:0
  },
  h1: {
    color: "#0098a7", fontWeight: "bold" ,marginBottom:10,fontSize: scale(24),
    marginTop: 10,
    textTransform:"uppercase",
    textAlign:"center"
  },
   h2: {
    color: "#0098a7", fontWeight: "bold" ,marginBottom:10,fontSize: scale(24),
    marginTop: 10,
    textTransform:"uppercase",
    textAlign:"center"
  },
});
