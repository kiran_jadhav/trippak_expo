import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  Linking,
  ScrollView
} from "react-native";
import { NavigationActions, HeaderBackButton } from "react-navigation";
import { customStyles } from "./../styles/main";
import UtilColors from '../utils/colors';

export default class aboutScreen extends React.Component {
  static navigationOptions = props => ({
    headerLeft: (
      <HeaderBackButton
        tintColor="#000"
        onPress={() => props.navigation.dispatch({ type: "Navigation/BACK" })}
      />
    ),
    headerTitle: "ABOUT US",
    headerTitleStyle: { color: "black" },
    headerTintColor: "black",
    headerStyle: {
      backgroundColor: UtilColors.titleColor,
    }
  });

  replenishProduct = () => {
    Linking.openURL("https://trippak.health");
  };

  render() {
    return (
      <View style={[customStyles.containerSuper, { backgroundColor: "#000" }]}>
        <ScrollView>
          <View
            style={[
              customStyles.containerContent,
              { backgroundColor: "#000", color: "#fff" }
            ]}
          >
          <Image
              style={[customStyles.imageAbtSquare, { marginBottom: 30 }]}
              source={require("../../assets/aboutus.png")}
            />

            <Text
              style={[
                customStyles.subheading,
                
                { color: "#fff" }
              ]}
            >
              Origin of Trippak
            </Text>
            <Text style={[customStyles.text, { color: "#fff" }]}>
               Trip Pak{"\u2122"}  was carefully designed, crafted and  compiled by a Physician with over 30 years experience as a comprehensive all inclusive stop gap measure for most acute and self limiting illnesses. Use the Trip Pak{"\u2122"} App to treat symptoms when ill using product information. It buys you and your family crucial time to help you get over the hump if you get sick while away, traveling to and fro, or at home. 

      Trip Pak{"\u2122"} offers peace of mind and tranquility in a  fashionable patent pending Bag.  Save money from expensive insurance co-pays and deductibles.  Avoid long waits in Urgent care and Emergency waiting rooms  for self limiting diseases without complications  using familiar OTC product directed care . Patented product locator in App will show you where to find the desired product in Trip Pak{"\u2122"}.
            </Text>


              <Text
              style={[
                customStyles.subheading,
                { fontWeight: "normal" },
                { color: "#fff" }
              ]}
            >
              Arrange for Virtual Physician Visit if all else fails
            </Text>
            <Text style={[customStyles.text, { color: "#fff" }]}>
               If you remain ill,have questions, and /or product information directs you to consult a physician, get immediate treatment, assistance and advice via a virtual live visit for many common Urgent and non-emergent concerns from a board certified Healthcare provider. 
            </Text>

            <Text
      style={[
        customStyles.subheading,
        { color: "#0098a7", fontWeight: "bold" ,marginBottom:10}
        ]}
        >
        {"\n"}WHAT WE DO
        </Text>

            <Image
              style={[
                customStyles.imageAbt,
                { borderColor: "#cccccc", borderWidth: 2 }
              ]}
              source={require("./../../assets/pak.png")}
            />
            <Text
              style={[
                customStyles.subheading,
                { color: "#fff", fontWeight: "normal" }
              ]}
            >
              {"\n"}Product Selection and Development
            </Text>
            <Text style={[customStyles.text, { color: "#fff" }]}>{"\n"}
              Our team  constantly keeps up with medical trends and technology . We strive to find better ways to help one select superior products to assist one in time of need. 
            </Text>
            <Text style={[customStyles.text, { color: "#fff" }]}>
               Improving the health of our clients in an ever-changing industry is our goal by using the most utilized Doctor and Pharmacy recommended over the counter branded agents . 
            </Text>
            <Text style={[customStyles.text, { fontWeight: "normal",color: "#919191" }]}>
              These products are not intended to diagnose, treat, cure or prevent any disease.
            </Text>
            
            <Image
              style={[
                customStyles.imageAbt,
                { borderColor: "#cccccc", borderWidth: 2 }
              ]}
              source={require("./../../assets/commitment.png")}
            />
            <Text
              style={[
                customStyles.subheading,
                { color: "#fff", fontWeight: "normal",color: "#fff" }
              ]}
            >
              {"\n"}Commitment to Safety and Innovation
            </Text>
            <Text style={[customStyles.text, { color: "#fff" }]}>{"\n"}
              Our company understands the technology and medicine in the health care industry is constantly progressing.  Our team of experienced professionals  stay current with the latest drug trends. 
            </Text>
            <Text
              style={[customStyles.text, {  color: "#fff" }]}
            >
              Do not exceed the recommended dosage of any product and defer to Ask the Doctor section or consult your physician .Visit Virtual Physician if you have any question(s).{" "}
            </Text>
            <Text style={[customStyles.text, { paddingBottom: 40,fontWeight: "normal",color: "#919191" }]}>
              Drug facts  including directions, uses,  and  warnings are included for all products in Trip Pak{"\u2122"} and product safety information should be read in all instances prior to use. 
            </Text>
            
          </View>
        </ScrollView>
        <TouchableHighlight
          style={customStyles.bottomBar}
          onPress={this.replenishProduct}
        >
          <Text style={customStyles.bottomBarText}>
            Click here to replenish a product
          </Text>
        </TouchableHighlight>
      </View>
    );
  }
}
