import React from "react";
import {
  StyleSheet,
  Text,
  ScrollView,
  Image,
  View,
  Linking,
  WebView,
  TouchableWithoutFeedback,
  Platform,
  FlatList
} from "react-native";

import { NavigationActions, HeaderBackButton } from "react-navigation";
import {
  RkButton,
  RkModalImg,
  RkText,
  RkGalleryImage
} from "react-native-ui-kitten";
import _ from "lodash";
import { customStyles } from "../styles/main";
import { data } from "./../data";
import { scale } from "../utils/scale";
import detectVideoUrl from "../utils/EmbedHelper";
import HTMLView from "react-native-htmlview";
import firebase from "./../FirebaseConnect";
import config from "./../config";
import UtilColors from '../utils/colors';

var database = firebase.database();

export default class detailScreen extends React.Component {
  static navigationOptions = props => ({
    headerLeft: (
      <HeaderBackButton
        tintColor="#000"
        onPress={() => props.navigation.dispatch({ type: "Navigation/BACK" })}
      />
    ),
    headerTitle: "DETAIL",
    headerTitleStyle: { color: "black" },
    headerTintColor: "black",
    headerStyle: {
      backgroundColor: UtilColors.titleColor,
    }
  });

  state = {
    data: undefined,
    setting: {}
  };

  ChangeImage = item => {
    this.setState({
      ...this.state,
      currentImage: item
    });
  };

  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    const product = this.props.navigation.getParam("id", 1);
    console.log("-------------------" + product);
    this.getRemoteProduct(product);
  };

  showProductLocatorScreen = () => {
    console.log('ddd');
    this.props.navigation.navigate("ProductLocatorScreen",{productLocator:this.state.data.product_locator,name:this.state.data.name.toUpperCase()});
  }

  getRemoteProduct(_id) {
    database.ref("products").on("value", snapshot => {
      let products = [];
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        products.unshift(childData);
      });
      console.log(_.find(products, x => x.id === _id));
      this.setState(
        {
          ...this.state,
          data: _.find(products, x => x.id === _id)
        },
        () => {
          this.getSetting();
        }
      );
    });
  }

  getSetting() {
    database.ref("setting").on("value", snapshot => {
      let setting = {};
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        //console.log(childKey + " x " + childData);
        setting[childKey] = childData; //({ childKey: childData });
      });

      this.setState(
        {
          ...this.state,
          setting: setting
        },
        () => {
          console.log(this.state);
          this.getEmbedVideo();
          //this.getSetting();
        }
      );
    });
  }

  getEmbedVideo = () => {
    let vidInfo = detectVideoUrl(this.state.data.embedVideo);
    if (typeof vidInfo.provider !== "undefined") {
      if (vidInfo.provider == "youtube") {
        this.setState({
          data: {
            ...this.state.data,
            embedURL: vidInfo.embed
          }
        });
      }
      if (vidInfo.provider == "vimeo") {
        this.setState({
          data: {
            ...this.state.data,
            embedURL: vidInfo.embed
          }
        });
      }
    }
  };

  _renderHeader = options => {
    return (
      <View>
        <RkText
          style={{
            textAlign: "center",
            marginTop: scale(0),
            fontSize: 18,
            padding: 0
          }}
        >
          {this.state.data.name.toUpperCase()}
        </RkText>
      </View>
    );
  };

  _renderFooter = options => {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          marginBottom: scale(10),
          height: scale(200)
        }}
      >
        <TouchableWithoutFeedback
          style={{ flex: 1, marginTop: 1 }}
          onPress={() => Linking.openURL(config.virtualPhysician)}
        >
          <Image
            style={{ width: 130, height: 130, flex: 1, marginTop: -50 }}
            resizeMode="contain"
            source={require("./../../assets/header_logo.png")}
          />
        </TouchableWithoutFeedback>

        <RkButton
          onPress={() => this.setState({ ...this.state, showLocator: false })}
          style={{
            backgroundColor: "#000",
            borderRadius: 0
          }}
        >
          CLOSE
        </RkButton>
      </View>
    );
  };

  renderNode(node, index, siblings, parent, defaultRenderer) {
    if (node.name == "li") {
      return (
        <View key={index} style={{ marginTop: index === 0 ? 10 : 0 }}>
          <View style={{ flexDirection: "row" }}>
            <View
              style={{ flexDirection: "column", marginLeft: 20, width: 20 }}
            >
              <Text>{"\u2022"}</Text>
            </View>
            <View style={{ flexDirection: "column", width: "90%" }}>
              <Text>{defaultRenderer(node.children, parent)}</Text>
            </View>
          </View>
        </View>
      );
    } else if (node.name == "ul") {
      return <View>{defaultRenderer(node.children, parent)}</View>;
    }
  }

  render() {
    const textes = !!this.state.data
      ? this.state.data.description.split("Need Help")
      : [];
    return (
      <ScrollView>
        {!!this.state.data && (
          <View style={customStyles.containerContent}>
            <Text style={[customStyles.subheading, { paddingBottom: 10 }]}>
              {this.state.data.name}
            </Text>
            <Image
              style={[customStyles.imageFull, { marginBottom: 10 }]}
              source={{
                uri: !!this.state.currentImage
                  ? this.state.currentImage
                  : this.state.data.photos[0]
              }}
            />

            <FlatList
              data={this.state.data.photos}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              renderItem={({ item }) => (
                <TouchableWithoutFeedback
                  key={item}
                  onPress={() => this.ChangeImage(item)}
                >
                  <Image
                    style={[customStyles.productImage]}
                    source={{ uri: item }}
                  />
                </TouchableWithoutFeedback>
              )}
            />

            <RkButton
              style={[customStyles.locator]}
              // onPress={() =>
              //   this.setState({ ...this.state, showLocator: true })
              // }
              onPress={this.showProductLocatorScreen}
            >
              PRODUCT LOCATOR
            </RkButton>

            
            {!!this.state.showLocator && (
              <View>
                <RkModalImg
                  visible={true}
                  modalImgStyle={{
                    flex: 1,
                    marginTop: scale(Platform.OS === "ios" ? 68 : 48),
                    marginBottom: scale(Platform.OS === "ios" ? 100 : 80),
                    resizeMode: "contain",
                    justifyContent: "center"
                  }}
                  source={
                    this.state.data.product_locator !== ""
                      ? { uri: this.state.data.product_locator }
                      : require("./../../assets/noimagefound.png")
                  }
                  renderHeader={this._renderHeader}
                  renderFooter={this._renderFooter}
                />
              </View>
            )}

            {!!textes[0] && (
              <View style={{ flex: 1, width: scale(320) }}>
                <HTMLView
                  style={{ flex: 1 }}
                  stylesheet={[styles]}
                  renderNode={this.renderNode}
                  value={textes[0]}
                />

                
              </View>
            )}

            <Text></Text>
            <Text></Text>

            {!!this.state.data.embedURL && (
              // <View style={{ flex: 1, width: scale(320) }}>
                <View style={[customStyles.shopBtn, {flex: 1, width: scale(320)}]}>
                <Text>
                  <Text style={{fontSize:16,padding:10}}>Need help?
                  </Text>
                  <Text> Not sure if this is the right product for you? Match your symptoms with assistance from video below:</Text>
                </Text>
              </View>
            )}

            {!!this.state.data.embedURL && (
              <WebView
                style={{
                  flex: 1,
                  backgroundColor: "#000",
                  width: scale(320),
                  height: scale(240),
                  marginBottom: scale(20),
                  marginTop: scale(20)
                }}
                javaScriptEnabled={true}
                source={{
                  uri: this.state.data.embedURL
                }}
              />
            )}

            <View style={[customStyles.shopBtn, { flex: 1, width: scale(320) }]}>
              <HTMLView
                style={{ flex: 1, fontWeight:'bold', fontSize:25}}
                stylesheet={[styles]}
                value={"<h4>" + this.state.setting.HelpText + "</h4>"}
              />
            </View>

            <RkButton
              style={[customStyles.locator, { marginTop: scale(10) }]}
              onPress={() => Linking.openURL(this.state.setting.Link)}
            >
              {!!this.state.setting.ButtonTitle &&
                this.state.setting.ButtonTitle.toUpperCase()}
            </RkButton>
          </View>
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  a: {
    color: "#000000",
    textAlign: "center"
  },
  ul: {
    paddingLeft: 10,
    paddingBottom: 10,
    fontSize: 10
  },
  li: {
    marginTop: 20
  },
  h5:{
    //display:"inline"
  }
});
