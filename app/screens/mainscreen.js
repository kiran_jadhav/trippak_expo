import React from "react";
import {
  FlatList,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  ScrollView,
  Keyboard,
  Platform,
  TouchableHighlight,
  Linking,
  StatusBar,
  ImageBackground
} from "react-native";
import _ from "lodash";
import { RkStyleSheet, RkText, RkTextInput } from "react-native-ui-kitten";
import { NavigationActions, HeaderBackButton } from "react-navigation";
import { data } from "./../data";
import { FontAwesome } from "../../assets/icons";

import { scale } from "./../utils/scale";
import { customStyles } from "../styles/main";
const { height, width } = Dimensions.get("window");
import firebase from "./../FirebaseConnect";
import UtilColors from '../utils/colors';
import { vw, vh, vmin, vmax } from 'react-native-expo-viewport-units';

var database = firebase.database();

RenderHeader = props => (
  <View style={styles.searchContainer}>
    <RkTextInput
      style={styles.searchBar}
      labelStyle={styles.searchLabel}
      autoCapitalize="none"
      autoCorrect={false}
      onChange={props.onSearchInputChanged}
      label={props.labelView()}
      rkType="rounded"
      value={props.defaultText}
      onSubmitEditing={props.onSearchSubmit}
      // placeholder="Search products by symptom"
      placeholder="Enter symptom here "
    />
  </View>
);

export default class mainScreen extends React.Component {
  // static propTypes = {
  //   navigation: NavigationType.isRequired,
  // };
  
  static navigationOptions = (props) => ({
    headerLeft: (
      <HeaderBackButton
        tintColor="#000"
        onPress={() => props.navigation.dispatch({ type: "Navigation/BACK" })}
      />
    ),
    headerTitle: "TRIPPAK\u2122",
    headerTitleStyle: { color: "black" },
    headerTintColor: "black",
    headerStyle: {
      backgroundColor: UtilColors.titleColor,
    }
  });

  state = {
    data: {
      categoryOriginal: null,
      categoryFilterd: null,
      original: null,
      filtered: null
    },
    searchResults: false,
    defaultSearchText: ""
  };

  componentDidMount = () => {
    this.getRemoteCategories();
  };

  getRemoteCategories() {
    database.ref("categories").on("value", snapshot => {
      let categories = [];
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        categories.unshift(childData);
      });
      this.setState(
        {
          ...this.state,
          data: {
            ...this.state.data,
            categoryOriginal: categories,
            categoryFilterd: categories
          }
        },
        () => {
          this.getRemoteProduct();
          console.log(this.state.data.categoryOriginal);
        }
      );
    });
  }

  getRemoteProduct() {
    database.ref("products").on("value", snapshot => {
      let products = [];
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        products.unshift(childData);
      });
      this.setState(
        {
          ...this.state,
          data: {
            ...this.state.data,
            original: products,
            filtered: _.sortBy(products, o => o.name)
          }
        },
        () => console.log(this.state.data.original)
      );
    });
  }

  replenishProduct = () => {
    Linking.openURL("https://trippak.health");
  };

  extractItemKey = item => `${item.id}`;

  onSearchInputChanged = event => {
    if (event.nativeEvent.text !== this.state.qry) {
      this.setState(
        {
          ...this.state,
          defaultSearchText: event.nativeEvent.text
        },
        () => this.retriveSuggection()
      );
    }

    //console.log(this.state.data.categoryOriginal);
  };

  retriveSuggection = () => {
    const pattern = new RegExp(this.state.defaultSearchText, "i");
    const categories = _.filter(this.state.data.categoryOriginal, category => {
      const filterResult = {
        Name: category.Name.search(pattern)
      };
      return filterResult.Name !== -1 ? category : undefined;
    });
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        categoryFilterd: categories
      },
      searchResults: true
    });
  };

  onSortProducts = categoryID => {
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        filtered: _.sortBy(this.findAll(categoryID + "", this.state.data.original), o => o.name)
      }
    });
  };

  showAllProducts = () => {
      this.setState({
        ...this.state,
        searchResults: false,
        data: {
          ...this.state.data,
          filtered: _.sortBy(this.state.data.original, o => o.name)
        }
      });
  };

  findAll = (id, items) => {
    var i = 0,
      result = [];

    for (; i < items.length; i++) {
      if (_.isArray(items[i].categories)) {
        if (items[i].categories.indexOf(id) > -1) {
          result.push(items[i]);
        }
      }
    }

    return result;
  };

  onItemPressed = item => {
    console.log("Detail " + item.id);
    this.props.navigation.navigate("Detail", { id: item.id });
    //this.props.navigation.navigate("Blog", { id: item.id });
  };

  sortProducts = item => {
    this.setState(
      {
        ...this.state,
        defaultSearchText: item.Name,
        searchResults: false
      },
      () => this.onSortProducts(item.id)
    );
    Keyboard.dismiss();
  };

  renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => this.onItemPressed(item)}>
      <View style={[styles.container]}>
        <Image style={[styles.avatar]} source={{ uri: item.photos[0] }} />
        <RkText style={[styles.title]}>{item.name}</RkText>
      </View>
    </TouchableOpacity>
  );

  renderSeparator = () => <View style={styles.separator} />;

  renderHeaderLabel = () => (
    <RkText rkType="awesome" style={{ marginLeft: 10 }}>
      {FontAwesome.search}
    </RkText>
  );

  render = () => (
    <View style={{ flex: 1 }}>
      <RenderHeader
        onSearchInputChanged={this.onSearchInputChanged}
        onSearchSubmit={this.showAllProducts}
        defaultText={this.state.defaultSearchText}
        labelView={this.renderHeaderLabel}
      />
      {!!this.state.searchResults &&
        this.state.data.categoryFilterd.length > 0 && (
          <ScrollView
            style={styles.autoCompleteResult}
            keyboardShouldPersistTaps={"always"}
          >
            {this.state.data.categoryFilterd.map((result, i) => {
              //console.log(result);
              //if (i > 3) return;
              return (
                <TouchableWithoutFeedback
                  key={i}
                  onPress={() => this.sortProducts(result)}
                >
                  <View style={styles.autoCompleteRow}>
                    <RkText
                      rkType="secondary3"
                      numberOfLines={2}
                      textBreakStrategy={"balanced"}
                      style={{ justifyContent: "center" }}
                    >
                      {result.Name.toString()}
                    </RkText>
                  </View>
                </TouchableWithoutFeedback>
              );
            })}
          </ScrollView>
        )}
        {this.state.defaultSearchText === "" ? 
        <ImageBackground
      source={require("../../assets/welcome_bg.png")}
      style={{
        flex: 1,
        //width: scale(360),
        // height: scale(440),
        width: vw(100), 
        height: vh(85),
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 20
      }}
    >
      <View
        style={{
          backgroundColor: "#44352c40",
          // backgroundColor: "#000000",
          padding: 8,
          flex: 1,
          width: scale(360),
          height: scale(440),
          alignItems: "center",
          justifyContent: "center",
          
        }}
      >

        <Text
          style={[
            customStyles.text,
            { fontSize: 20, fontWeight: "bold", paddingLeft: 10, paddingRight: 10 },
            { color: "#fff" }
          ]}
        >
          Search products by entering symptoms in above box (eg - Cough)
        </Text>
      </View>
      
    </ImageBackground> : <FlatList
        style={styles.root}
        data={this.state.data.filtered}
        renderItem={this.renderItem}
        //ListHeaderComponent={this.renderHeader}
        //stickyHeaderIndices={}
        ItemSeparatorComponent={this.renderSeparator}
        keyExtractor={this.extractItemKey}
        horizontal={false}
        numColumns={2}
        enableEmptySections
      />
          }
      
      <TouchableHighlight
        style={customStyles.bottomBar}
        onPress={this.replenishProduct}
      >
        <Text style={customStyles.bottomBarText}>
          Click here to replenish a product
        </Text>
      </TouchableHighlight>
    </View>
  );
}

const styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base,
    marginBottom: 20
  },
  searchContainer: {
    backgroundColor: "#f6f6f6",
    paddingHorizontal: 16,
    paddingVertical: 5,
    height: 60,
    alignItems: "center"
  },
  container: {
    flexDirection: "column",
    alignItems: "center"
  },
  searchBar: {
    height: scale(35)
  },
  searchLabel: {
    fontSize: 13,
  },
  avatar: {
    margin: 20,
    width: (width - 80) / 2,
    height: 150,
    resizeMode: "contain"
  },
  title: {
    fontSize: 13,
    width: (width - 32) / 2,
    height: 40,
    alignItems: "center",
    textAlign: "center"
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: theme.colors.border.base
  },
  autoCompleteResult: {
    position: "absolute",
    top: Platform.OS === "ios" ? 60 : 60,
    width: scale(350),
    height: 240,
    zIndex: 999,
    borderColor: "#ccc",
    borderTopWidth: 1
  },
  autoCompleteRow: {
    width: scale(350),
    flexDirection: "row",
    height: 40,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: "#f1f1f1",
    fontSize: 18,
    elevation: 2,
    shadowRadius: 5,
    borderWidth: 1,
    borderColor: "#ccc",
    borderTopWidth: 0
  }
}));
