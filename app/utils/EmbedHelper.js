export default url => {
  // - Supported YouTube URL formats:
  //   - http://m.youtube.com/watch?v=My2FRPA3Gf8
  //   - http://www.youtube.com/watch?v=My2FRPA3Gf8
  //   - http://youtu.be/My2FRPA3Gf8
  //   - https://youtube.googleapis.com/v/My2FRPA3Gf8
  // - Supported Vimeo URL formats:
  //   - http://vimeo.com/25451551
  //   - http://player.vimeo.com/video/25451551
  // - Also supports relative URLs:
  //   - //player.vimeo.com/video/25451551
  //console.log("Embed Video:" + url);
  if ( !url ||
    url.match(
      /(http:|https:|)\/\/(player.|www.|m.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/
    ) == null
  )
    return {
      provider: undefined,
      id: undefined,
      embed: undefined
    };

  //console.log("==>" + RegExp.$3);

  if (RegExp.$3.indexOf("youtu") > -1) {
    var type = "youtube";
    var embed =
      "https://www.youtube.com/embed/" +
      RegExp.$6 +
      "?rel=0&amp;autoplay=0&amp;controls=1&amp;showinfo=0&amp;fs=0&amp;playsinline=1";
  } else if (RegExp.$3.indexOf("vimeo") > -1) {
    var type = "vimeo";
    var embed =
      "https://player.vimeo.com/video/" +
      RegExp.$6 +
      "?autoplay=0&amp;title=0&amp;byline=0&amp;portrait=0";
  } else {
    return undefined;
  }

  return {
    provider: type,
    id: RegExp.$6,
    embed: embed
  };
};
