const images = [
  require("../img/Image10.png"),
  require("../img/Image11.png"),
  require("../img/Image2.png"),
  require("../img/Image3.png"),
  require("../img/Image4.png"),
  require("../img/Image1.png"),
  require("../img/Image12.png"),
  require("../img/Image8.png"),
  require("../img/Image6.png"),
  require("../img/Image9.png"),
  require("../img/Image5.png"),
  require("../img/Image7.png")
];

const products = [
  {
    id: "1",
    name: "Imodium A-D",
    sku: "MDM-A-D",
    photos: [
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/MDM-A-D-1.jpg?alt=media&token=3ab2b653-5efe-4511-8cde-02154b706622",
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/MDM-A-D-2.jpg?alt=media&token=ed5785e1-43f4-44f5-b229-92b1a75c8051"
    ],
    price: "6.00",
    product_url: "",
    description:
      "Antidiarrheal Agent\r\nUses:Controls symptoms of diarrhea including Travelers diarrhea\r\nNeed Help? Not sure if this product is for you? Match your symptoms with the assistance of the video below: \r\nhttps://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjCjKKo2qbgAhXjo4MKHTwXAMUQMwjbASg9MD0&url=https%3A%2F%2Fwww.ispot.tv%2Fad%2FAcwg%2Fimodium-a-d-follow-a-rhythm&psig=AOvVaw2Sc-r8BaeJpAoMJj55fQ1q&ust=1549528685831836&ictx=3&uact=3",
    categories: ["57", "59"]
  },
  {
    id: "2",
    name: "Tylenol Cold and Flu Severe",
    sku: "TYL-CLD-AND-FLU",
    photos: [
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/TYL-CLD-AND-FLU.jpg?alt=media&token=fafde1ba-3670-4e21-b045-6eda71ec9430",
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/TYL-CLD-AND-FLU-2.jpg?alt=media&token=5d70860a-9cd4-4619-9a08-45f94509c9b1",
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/TYL-CLD-AND-FLU-3.jpg?alt=media&token=7db1fedf-41fc-4fe8-b67f-0672ced3cf7f"
    ],
    price: "6.00",
    product_url: "",
    description:
      "Temporary relief of the following cold/flu symptoms:\r\nMinor aches and pains\r\nHeadache\r\nSore throat\r\nNasal congestion\r\nCough\r\nHelps loosen phlegm(mucus) and the bronchial secretions to make cough more productive\r\nFever reducer\r\nNeed Help? Not sure if this product is for you? Match your symptoms with the assistance of the video below: \r\nhttps://www.ispot.tv/ad/A7dy/tylenol-cold-flu-severe-everything-youve-got\r\n\r\nhttps://www.ispot.tv/ad/A7dy/tylenol-cold-flu-severe-everything-youve-got",
    categories: [
      "0",
      "2",
      "3",
      "20",
      "24",
      "27",
      "46",
      "50",
      "70",
      "87",
      "90",
      "91",
      "111",
      "112"
    ]
  },
  {
    id: "3",
    name: "Cold 'n Hot Pain Relief Medicated Patch",
    sku: "CLD-N-HOT",
    photos: [
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/CLD-N-HOT-1.jpg?alt=media&token=041b3aa3-5598-42bc-abca-f86b3bec9f97",
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/CLD-N-HOT-2.jpg?alt=media&token=ebd36cb1-2b95-4263-be11-66c9d17f478e"
    ],
    price: "6.00",
    product_url: "",
    description:
      "Pain relief Medicated Patch\r\nTemporarily relieves minor pain associated with:\r\nArthritis\r\nBackache\r\nBursitis \r\nTendonitis\r\nMuscular strains\r\nBruises \r\nCramps",
    categories: [
      "2",
      "24",
      "25",
      "52",
      "53",
      "61",
      "63",
      "64",
      "86",
      "89",
      "94",
      "122"
    ]
  },
  {
    id: "4",
    name: "Blink Lid Wipes",
    sku: "BLN-LID-WPS",
    photos: [
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/BLN-LID-WPS-1.jpg?alt=media&token=ccca5dff-675a-4b30-9d65-39269b208e1d",
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/BLN-LID-WPS-2.jpg?alt=media&token=940c892f-eea7-40d2-9d39-c7d1b3fb12bb"
    ],
    price: "6.00",
    product_url: "",
    description:
      "A sterile hypoallergenic cleansing eye-lid wipe for tired dry feeling eyes without eyedrops\r\nCleans,moisturizes and soothes eye-lids\r\nRemoves make up and debris from eyelids and eyelashes\r\nFormulated with soothing camomile to remove debris and other secretions\r\nHigh quality embossed wipe to gently massage and exfoliate\r\nNo need to rinse \u2013 just clean and go\r\nSuitable for use with babies, children and adults, including contact lens wearers\r\n Need Help? Not sure if this product is for you? Match your symptoms with the assistance of the video below:   \r\nhttps://www.youtube.com/watch?v=PtRk50SOL_M",
    categories: ["6", "43", "72", "102", "104", "107", "116"]
  },
  {
    id: "5",
    name: "Blink Lid Wipes 5",
    sku: "BLN-LID-WPS",
    photos: [
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/BLN-LID-WPS-1.jpg?alt=media&token=ccca5dff-675a-4b30-9d65-39269b208e1d",
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/BLN-LID-WPS-2.jpg?alt=media&token=940c892f-eea7-40d2-9d39-c7d1b3fb12bb"
    ],
    price: "6.00",
    product_url: "",
    description:
      "A sterile hypoallergenic cleansing eye-lid wipe for tired dry feeling eyes without eyedrops\r\nCleans,moisturizes and soothes eye-lids\r\nRemoves make up and debris from eyelids and eyelashes\r\nFormulated with soothing camomile to remove debris and other secretions\r\nHigh quality embossed wipe to gently massage and exfoliate\r\nNo need to rinse \u2013 just clean and go\r\nSuitable for use with babies, children and adults, including contact lens wearers\r\n Need Help? Not sure if this product is for you? Match your symptoms with the assistance of the video below:   \r\nhttps://www.youtube.com/watch?v=PtRk50SOL_M",
    categories: ["6", "43", "72", "102", "104", "107", "116"]
  },
  {
    id: "56",
    name: "Blink Lid Wipes 55",
    sku: "BLN-LID-WPS",
    photos: [
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/BLN-LID-WPS-1.jpg?alt=media&token=ccca5dff-675a-4b30-9d65-39269b208e1d",
      "https://firebasestorage.googleapis.com/v0/b/trippak-ffce8.appspot.com/o/BLN-LID-WPS-2.jpg?alt=media&token=940c892f-eea7-40d2-9d39-c7d1b3fb12bb"
    ],
    price: "6.00",
    product_url: "",
    description:
      "A sterile hypoallergenic cleansing eye-lid wipe for tired dry feeling eyes without eyedrops\r\nCleans,moisturizes and soothes eye-lids\r\nRemoves make up and debris from eyelids and eyelashes\r\nFormulated with soothing camomile to remove debris and other secretions\r\nHigh quality embossed wipe to gently massage and exfoliate\r\nNo need to rinse \u2013 just clean and go\r\nSuitable for use with babies, children and adults, including contact lens wearers\r\n Need Help? Not sure if this product is for you? Match your symptoms with the assistance of the video below:   \r\nhttps://www.youtube.com/watch?v=PtRk50SOL_M",
    categories: ["6", "43", "72", "102", "104", "107", "116"]
  }
];

export default products;
