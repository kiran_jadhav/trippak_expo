import _ from "lodash";
import populate from "./dataGenerator";
import users from "./raw/users";
//import categories from "./raw/categories";
//import products from "./raw/products";
import articles from "./raw/articles";
import notifications from "./raw/notifications";
import conversations from "./raw/conversations";
import cards from "./raw/cards";
import firebase from "./../FirebaseConnect";

var database = firebase.database();


class DataProvider {

  

  getRemoteCategories () {

    database.ref("categories").on("value", snapshot => {
      let categories = [];
      snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        categories.unshift(childData);
      });
      return categories
    });

  }


  getProducts() {

    database.ref("products").on("value", snapshot => {
      let products = [];
      snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        console.log(childData);
        products.unshift(childData);
      });
      return products
    });

  }

  getUser(id = 1) {
    return _.find(users, x => x.id === id);
  }

  getUsers() {
    return users;
  }

  getProduct(id = 1) {
    return _.find(getProducts(), x => x.id === id);
  }

  getProductsx() {
    return this.products;
  }

  getCategory(id = 1) {
    return _.find(categories, x => x.id === id);
  }

  getCategories() {
    database.ref("categories").on("value", snapshot => {
      let categories = [];
      snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        categories.unshift(childData);
      });
      console.log(categories)
    });

    return categories
  }

  getNotifications() {
    return notifications;
  }

  getArticles(type = "article") {
    return _.filter(articles, x => x.type === type);
  }

  getArticle(id) {
    return _.find(articles, x => x.id === id);
  }

  getConversation(userId = 1) {
    return _.find(conversations, x => x.withUser.id === userId);
  }

  getChatList() {
    return conversations;
  }

  getComments(postId = 1) {
    return this.getArticle(postId).comments;
  }

  getCards() {
    return cards;
  }

  populateData() {
    populate();
  }
}

export const data = new DataProvider();
