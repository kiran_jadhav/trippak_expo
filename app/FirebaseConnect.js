import firebase from "firebase";
import config from "./config";

const configFirebase = {
  apiKey: config.apiKey,
  authDomain: config.authDomain,
  databaseURL: config.databaseURL,
  storageBucket: config.storageBucket  
};

export default (!firebase.apps.length
  ? firebase.initializeApp(configFirebase)
  : firebase.app());
