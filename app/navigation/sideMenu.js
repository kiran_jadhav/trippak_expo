import React from "react";
import {
  TouchableHighlight,
  View,
  ScrollView,
  Linking,
  Image,
  Platform,
  StyleSheet
} from "react-native";
import { RkStyleSheet, RkText, RkTheme } from "react-native-ui-kitten";
import { FontIcons } from "../../assets/icons";
//import { MainRoutes } from "../../config/navigation/routes";
import { FontAwesome } from "../../assets/icons";
import aboutScreen from "./../screens/aboutscreen";
import welcomeScreen from "./../screens/welcomescreen";
import mainScreen from "./../screens/mainscreen";
import blogScreen from "./../screens/blogscreen";
import pharmacyScreen from "./../screens/pharmacyscreen";
import firebase from "./../FirebaseConnect";
var database = firebase.database();
var DefaultRoutes = [
  {
    id: "Welcome",
    title: "Welcome",
    screen: "Welcome",
    icon: FontAwesome.dashboard,
    order:"1"
  },
  {
    id: "About",
    title: "About Us",
    screen: "About",
    icon: FontAwesome.about,
    order:"7"
  },
  {
    id: "Pharmacy",
    title: "Pharmacy",
    screen: "PharmacyScreen",
    icon: FontAwesome.about,
    order:"3"
  },
  {
    id: "ProductSearch",
    title: "Search by Symptoms",
    screen: "ProductSearch",
    icon: FontAwesome.search,
    order:"4"
  }
];

var MainRoutes = [];

export default class SideMenu extends React.Component {

  _isMounted = false;

  constructor(props) {
    // Required step: always call the parent class' constructor
    super(props);

    // Set the state directly. Use props if necessary.
    this.state = {
      isOpen: false,
      plusMinus: FontAwesome.plus,

      // Note: think carefully before initializing
      // state based on props!
     // someInitialValue: this.props.initialValue
    }}
  getBlogTitles = false;

  onMenuItemPressed = item => {
    if (item.id == "Welcome") this.props.navigation.closeDrawer();
    else if (item.id == "Browser") Linking.openURL(item.screen);
    else if (item.id == "ProductSearch") this.props.navigation.navigate("ProductSearch");
    else if (!!item.param) this.props.navigation.navigate(item.screen, item.param);
    else this.props.navigation.navigate(item.screen);
  };

  getThemeImageSource = theme =>
    theme.name === "light"
      ? require("../../assets/logo.png")
      : require("../../assets/logo.png");

  renderIcon = () => (
    <Image
      style={styles.icon}
      source={this.getThemeImageSource(RkTheme.current)}
    />
  );

  componentDidMount = () => {
    //if(!this.getBlogTitles)
    this._isMounted = true;
    this.getSetting();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  openDropdown = () => {
  
    if (this._isMounted) {
      if (this.state.isOpen == false) {
        this.setState({isOpen:true});
        this.setState({plusMinus:FontAwesome.minus});
      } else {
        this.setState({isOpen:false});
        this.setState({plusMinus:FontAwesome.plus});
      }
    }
  }

  getSetting() {
    database.ref("setting").on("value", snapshot => {
      let setting = {};
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        //console.log(childKey + " x " + childData);
        setting[childKey] = childData; //({ childKey: childData });
      });

      if (this._isMounted) {
        this.setState(
          {
            ...this.state,
            setting: setting
          },
          () => {
            this.getRemoteBlogs();
            //console.log(this.state);
            //this.getSetting();
          }
        );
      }
    });
  }

  getRemoteBlogs() {
    MainRoutes = [];

    DefaultRoutes.map(item => MainRoutes.push(item));

    
    MainRoutes.push({
      id: "Browser",
      title: this.state.setting.ButtonTitle,
      screen: this.state.setting.Link,
      icon: FontAwesome.link,
      order:5
    });
    //MainRoutes.merge(DefaultRoutes);

    database.ref("blog").on("value", snapshot => {
      let blogs = [];
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        MainRoutes.push({
          id: "Blog " + childData.id,
          title: childData.title,
          param: { id: childData.id },
          screen: "Blog",
          icon: FontAwesome.blog,
          order: childData.order
        });
      });

      // MainRoutes.push(blogs);
     // console.log(MainRoutes);
      this.getBlogTitles = true;
    });

    //order the list by order number
    MainRoutes.sort((a, b) => (a.order > b.order) ? 1 : -1)

  }

  renderMenu = () => MainRoutes.map(this.renderMenuItem,this);

  renderMenuItem = item => { 
    if(item.order == 1 || 
      item.order == 2 ||
      item.order == 3 ||
      item.order == 4 ||
      item.order == 5 ||
       item.order == 6 ||
       item.order == 7){
    return (
    <TouchableHighlight
      style={styles.container}
      key={item.id}
      underlayColor={RkTheme.current.colors.button.underlay}
      activeOpacity={1}
      onPress={() => this.onMenuItemPressed(item)}
    >
      <View style={styles.content}>
        <View style={styles.content}>
          <RkText style={styles.icon} rkType="awesome primary small">
            {item.icon}
          </RkText>
          <RkText rkType="small">{item.title}</RkText>
        </View>
        <RkText rkType="awesome secondaryColor small">
          {FontAwesome.chevronRight}
        </RkText>
      </View>
    </TouchableHighlight>
  )}

  if(item.order == 8){ return (<View>
     <TouchableHighlight
      style={styles.container}
      key={item.id}
      underlayColor={RkTheme.current.colors.button.underlay}
      activeOpacity={1}
      onPress={() => this.onMenuItemPressed(item)}
    >
      <View style={styles.content}>
        <View style={styles.content}>
          <RkText style={styles.icon} rkType="awesome primary small">
            {item.icon}
          </RkText>
          <RkText rkType="small">{item.title}</RkText>
        </View>
        <RkText rkType="awesome secondaryColor small">
          {FontAwesome.chevronRight}
        </RkText>
      </View>
    </TouchableHighlight>
    <TouchableHighlight
      style={styles.container}
      key={item.id}
      underlayColor={RkTheme.current.colors.button.underlay}
      activeOpacity={1}
      onPress={() => this.openDropdown()}
    >
      <View style={styles.content}>
        <View style={styles.content}>
          <RkText style={styles.icon} rkType="awesome primary small">
            {item.icon}
          </RkText>
          <RkText rkType="small">Articles</RkText>
        </View>
        <RkText rkType="awesome secondaryColor large">
          {this.state.plusMinus}
        </RkText>
      </View>
    </TouchableHighlight>
  </View>)}

    if(item.order >8){
    return (this.state.isOpen &&
    <TouchableHighlight
      style={styles.container}
      key={item.id}
      underlayColor={RkTheme.current.colors.button.underlay}
      activeOpacity={1}
      onPress={() => this.onMenuItemPressed(item)}
    >
      <View style={styles.content}>
        <View style={styles.content}>
          <RkText style={styles.icon} rkType="awesome primary small">
            {item.icon}
          </RkText>
          <RkText rkType="small">{item.title}</RkText>
        </View>
        <RkText rkType="awesome secondaryColor small">
          {FontAwesome.chevronRight}
        </RkText>
      </View>
    </TouchableHighlight>
  )}


};

  render = () => (
    <View style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {this.renderMenu()}
        
      </ScrollView>
    </View>
  );
}

const styles = RkStyleSheet.create(theme => ({
  container: {
    height: 60,
    paddingHorizontal: 16,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.border.base
  },
  root: {
    paddingTop: Platform.OS === "ios" ? 20 : 0,
    backgroundColor: theme.colors.screen.base
  },
  content: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  icon: {
    marginRight: 13,
    color: "#ebcc89"
  }
}));
